﻿var LSE;
(function (LSE) {
    (function (ProQuote) {
        (function (Services) {
            
            var dataService = (function(){

                // name is useful for logging
                var serviceId = 'dataService';

                // Define the controller on the module.
                // Inject the dependencies. 
                // Point to the controller definition function.
                angular.module('OrderEntryApp').service(serviceId, ['$http', '$log', '$location', '$q', dataService]);

                function dataService($http, $log, $location, $q) {
                    this.$http = $http;
                    this.$log = $log;
                    this.$location = $location;
                    this.$q = $q;
                }

                ////////////////////////////////////
                // GET - ALL Instruments
                ////////////////////////////////////
                dataService.prototype.instruments = function () {
                    var deferred = this.$q.defer();
                    var config = {};
                    this.$http.get('/api/instruments', config).success(function (data, status, headers, config) {
                        deferred.resolve(data);
                    }).error(function (data, status, headers, config) {
                        deferred.reject(status);
                    });
                    return deferred.promise;
                };


                ////////////////////////////////////
                // POST - ORDER PLACEMENT
                ////////////////////////////////////
                dataService.prototype.submitPlacement = function (placement) {
                    var deferred = this.$q.defer();
                    var payload = {InstrumentId: placement.instrumentId,Quantity:placement.quantity,Side:placement.side,OrderType:placement.orderType, TimeInForce:placement.tif,AlternativeSettlementCurrency:placement.altSettlementCurrency};
                    var requestString = '/api/orders';

                    this.$http.post(requestString, payload).success(function (data, status, headers, config) {
                        deferred.resolve(data);
                    }).error(function (data, status, headers, config) {
                        deferred.reject(status);
                    });
                    return deferred.promise;
                };

                ////////////////////////////////////
                // GET - ALL Orders
                ////////////////////////////////////
                dataService.prototype.orders = function () {
                    var deferred = this.$q.defer();
                    var config = {};
                    this.$http.get('/api/orders', config).success(function (data, status, headers, config) {
                        deferred.resolve(data);
                    }).error(function (data, status, headers, config) {
                        deferred.reject(status);
                    });
                    return deferred.promise;
                };
            })();

            Services.dataService = dataService;
            
        })(ProQuote.Services || (ProQuote.Services = {}));
    })(LSE.ProQuote || (LSE.ProQuote = {}));
})(LSE || (LSE = {}));
