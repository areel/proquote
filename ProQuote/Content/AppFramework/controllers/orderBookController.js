﻿var LSE;
(function (LSE) {
    (function (ProQuote) {
        (function (Controllers) {
            'use strict';
            
            var orderBookController = (function () {

                // useful for logging
                var controllerId = 'orderBookController';

                // Define the controller on the Application.
                // Inject the dependencies. 
                angular.module('OrderEntryApp').controller(controllerId, ['$scope', 'dataService', orderBookController]);


                function orderBookController($scope, dataService) {
                    this.$scope = $scope;
                    this.$scope.title = 'orderBookController';
                    this.$scope.dataService = dataService;
                    this.retrieveOrders();
                    this.defineGridOptions();
                }

                orderBookController.prototype.retrieveOrders = function () {

                    var myself = this;
                    this.$scope.dataService.orders().then(function (data) {
                        myself.retrieveOrdersSuccess(data);
                    }, function (status) {
                        myself.retrieveOrdersFailure(status);
                    });
                };

                orderBookController.prototype.retrieveOrdersSuccess = function (data) {
                    this.$scope.orders = data;
                };

                orderBookController.prototype.retrieveOrdersFailure = function (data) {
                    // todo Log and Display Failure
                    this.$scope.orders = [];
                };
                orderBookController.prototype.defineGridOptions = function () {
                    this.$scope.filterOptions = {
                        filterText: ''
                    };
                    this.$scope.gridOptions = {
                        data: 'orders',
                        enableCellSelection: false,
                        enableRowSelection: true,
                        keepLastSelected: false,
                        enableCellEditOnFocus: false,
                        selectedItems: [],
                        disableTextSelection: true,
                        multiSelect: false,
                        columnDefs: [
                            { field: 'InstrumentId', displayName: 'Instrument Id', width: 80 },
                            { field: 'OrderId', displayName: 'Order Id', width: 80 },
                            { field: 'Quantity', displayName: 'Quantity', width: 100 },
                            { field: 'QuantityUnit', displayName: 'Quantity Unit', width: 100 },
                            { field: 'Side', displayName: 'Side', width: 80 },
                            { field: 'OrderType', displayName: 'Order Type', width: 100 },
                            { field: 'TimeInForce', displayName: 'Time In Force', width: 100 },
                            { field: 'AlternativeSettlementCurrency', displayName: 'Alt Currency', width: 100 }
                        ],
                        filterOptions: this.$scope.filterOptions
                    };
                };

                return orderBookController;
            })();

            Controllers.orderBookController = orderBookController;
        })(ProQuote.Controllers || (ProQuote.Controllers = {}));
    })(LSE.ProQuote || (LSE.ProQuote = {}));
})(LSE || (LSE = {}));
