﻿var LSE;
(function (LSE) {
    (function (ProQuote) {
        (function (Controllers) {
            'use strict';

            var orderEntryController = (function () {

                // useful for logging
                var controllerId = 'orderEntryController';

                // Define the controller on the module.
                // Inject the dependencies. 
                // Point to the controller definition function.
                angular.module('OrderEntryApp').controller(controllerId, ['$scope', 'dataService', orderEntryController]);

                function orderEntryController($scope, dataService) {
                    var myself = this;
                    this.$scope = $scope;
                    this.$scope.title = 'orderEntryController';
                    this.$scope.dataService = dataService;
                    this.$scope.instrumentId = 0;
                    this.$scope.rowSelected = false;
                    
                    this.$scope.rowSelection = function () {
                        myself.$scope.rowSelected = myself.$scope.gridOptions.selectedItems.length > 0;

                        if (myself.$scope.rowSelected)
                            myself.$scope.instrumentId = myself.$scope.gridOptions.selectedItems[0].InstrumentId;
                        else {
                            myself.$scope.instrumentId = 0;
                        }
                    };

                    this.retrieveInstruments();
                    this.defineGridOptions();
                }

                orderEntryController.prototype.retrieveInstruments = function () {

                    var myself = this;
                    this.$scope.dataService.instruments().then(function (data) {
                        myself.retrieveInstrumentsSuccess(data);
                    }, function (status) {
                        myself.retrieveInstrumentsFailure(status);
                    });
                };

                orderEntryController.prototype.retrieveInstrumentsSuccess = function (data) {
                    this.$scope.instruments = data;
                };

                orderEntryController.prototype.retrieveInstrumentsFailure = function (data) {
                    // todo Log and Display Failure
                    this.$scope.instruments = [];
                };
                orderEntryController.prototype.defineGridOptions = function () {
                    this.$scope.filterOptions = {
                        filterText: ''
                    };
                    this.$scope.gridOptions = {
                        data: 'instruments',
                        enableCellSelection: false,
                        enableRowSelection: true,
                        keepLastSelected: false,
                        enableCellEditOnFocus: false,
                        selectedItems:[],
                        disableTextSelection: true,
                        multiSelect: false,
                        columnDefs: [
                            { field: 'InstrumentId', displayName: 'Instrument Id', width: 80},
                            { field: 'IssuerName', displayName: 'Issuer', width: 200 },
                            { field: 'IssuerCode', displayName: 'Issuer Code', width: 100 },
                            { field: 'Description', displayName: 'Description', width: 200 },
                            { field: 'Sector', displayName: 'Sector', width: 80 },
                            { field: 'Exchange', displayName: 'Exchange', width: 100 },
                            { field: 'SegmentCode', displayName: 'Segment Code', width: 100 },
                            { field: 'IsinCode', displayName: 'ISIN', width: 200 },
                            { field: 'MinimumOrderSize', displayName: 'Min Order', width: 80 },
                            { field: 'SecurityType', displayName: 'Security Type', width: 100 },
                            { field: 'Tidm', displayName: 'TIDM', width: 100 },
                            { field: 'Sedol', displayName: 'SEDOL', width: 100 },
                            { field: 'Currency', displayName: 'Currency', width: 50 },
                            { field: 'QuotationUnit', displayName: 'Quote Unit', width: 50 }
                        ],
                        filterOptions: this.$scope.filterOptions
                    };
                };

                return orderEntryController;
            })();
            Controllers.orderEntryController = orderEntryController;
        })(ProQuote.Controllers || (ProQuote.Controllers = {}));
    })(LSE.ProQuote || (LSE.ProQuote = {}));
})(LSE || (LSE = {}));
