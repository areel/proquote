﻿var LSE;
(function(LSE) {
    (function(ProQuote) {
        (function(Controllers) {
            'use strict';

            // logging
            var controllerId = 'homeController';

            // Define the controller on the module.
            // Inject the dependencies. 
            // Point to the controller definition function.
            angular.module('OrderEntryApp').controller(controllerId,['$scope', homeController]);

            function homeController($scope) {
                // Bindable properties and functions are placed on vm.
                $scope.title = 'homeController';
                
            }
            Controllers.homeController = homeController;
        })(ProQuote.Controllers || (ProQuote.Controllers = {}));
    })(LSE.ProQuote || (LSE.ProQuote = {}));
})(LSE || (LSE = {}));
