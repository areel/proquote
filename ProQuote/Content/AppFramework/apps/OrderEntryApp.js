﻿var LSE;
(function (LSE) {
    (function (ProQuote) {
        'use strict';

        // Create the module and define its dependencies.
        ProQuote.OrderEntryApp = angular.module('OrderEntryApp', ['ngAnimate', 'ngRoute', 'ngGrid', 'toaster']);

        ProQuote.OrderEntryApp.config(function ($locationProvider) {
            $locationProvider.html5Mode(true);
        }).config(function ($httpProvider) {
            $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
        }).config(function ($routeProvider) {
            $routeProvider.when('/', {
                templateUrl: '/Content/AppFramework/templates/home.html',
                controller: 'homeController'
            }).when('/orderEntry', {
                templateUrl: '/Content/AppFramework/templates/orderEntry.html',
                controller: 'orderEntryController'
            }).when('/orderBook', {
                templateUrl: '/Content/AppFramework/templates/orderBook.html',
                controller: 'orderBookController'
            }).when('/notes', {
                templateUrl: '/Content/AppFramework/templates/notes.html',
                controller: 'orderBookController'
            });;
        });
        
        // Execute bootstrapping code and any dependencies.
        ProQuote.OrderEntryApp.run(['$q', '$rootScope',
            function ($q, $rootScope) {
                // eye candy: show progress on submit buttons
                
            }]);
    })(LSE.ProQuote || (LSE.ProQuote = {}));
})(LSE || (LSE = {}));