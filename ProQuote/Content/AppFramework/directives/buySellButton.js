﻿// Present Radio Buttons in the style of default push Buttons
// Provide access to the text of the selected Button.

(function () {
    'use strict';

    angular.module('OrderEntryApp').directive('buySellButton',[buySellButton]);

    function buySellButton() {
        return {
            restrict: 'E',
            scope: { model: '=', options: '=' },
            controller: function ($scope) {
                $scope.activate = function (option) {
                    $scope.model = option;
                };
            },
            template: '<div class="ui-group-buttons">' +
                '<button type="button" class="btn btn-success btn-sm" ng-click="activate(' + "'Buy'" + ')">Buy</button>' +
                '<div class="or or-sm"></div>' +
                '<button type="button" class="btn btn-danger btn-sm" ng-click="activate(' + "'Sell'" + ')">Sell</button>' +
                '</div>'

        };
    }

})();