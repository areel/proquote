﻿var LSE;
(function (LSE) {
    (function (ProQuote) {
        (function (Directives) {
            angular.module('OrderEntryApp').directive('placement', ['$window', placement]);
            function placement($compile) {
                return {
                    scope: {
                        instrument: '=',
                    },
                    templateUrl:'/Content/AppFramework/directives/placement/placement.html',
                    restrict: 'E',
                    controller: function ($scope, $element, $attrs, $location, dataService,toaster) {
                        
                        $scope.dataService = dataService;
                        $scope.toaster = toaster;
                        $scope.sideModel = "Buy";
                        $scope.sideOptions = ["Buy", "Sell"];
                        $scope.placement = {};
                        $scope.submitPlacement = function (item, event) {
                            Ladda.bind('input[type=submit');
                            // todo use $element to locate button and not document
                            var laddaButton = Ladda.create(document.querySelector('#submitPlacementBtn'));
                            laddaButton.start();
                            $scope.placement.instrumentId = $scope.instrument;
                            $scope.placement.side = $scope.sideModel;

                            $scope.dataService.submitPlacement($scope.placement).then(function (data) {
                                laddaButton.stop();
                                $scope.toaster.pop('success', "Order Placement", "Order successfully placed.", 2000);
                            }, function (status) {
                                laddaButton.stop();
                                $scope.toaster.pop('warning', "Order Placement", "Received Error Code: " + status);
                            });
                        };
                    }
                };
            }
            Directives.placement = placement;

        })(ProQuote.Directives || (ProQuote.Directives = {}));
    })(LSE.ProQuote || (LSE.ProQuote = {}));
})(LSE || (LSE = {}));
