using ProQuote.Domain;

namespace ProQuote.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ProQuote.Infrastructure.DataSource>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(ProQuote.Infrastructure.DataSource context)
        {
            context.Instruments.AddOrUpdate(instrument => instrument.InstrumentId,
                new Instrument()
                {
                    InstrumentId = 124171,
                    Exchange = "LSE",
                    SegmentCode = "CWTR",
                    IsinCode = "XS0758038425",
                    MinimumOrderSize = 1,
                    SecurityType = "ML",
                    Tidm = "12NQ",
                    Description = "HSBC BK. 13",
                    Sedol = "B7ZN4Y4",
                    Currency = "USD",
                    IssuerCode = "HSBCB",
                    IssuerName = "HSBC BANK PLC",
                    QuotationUnit = 100,
                    Sector = "UIDW"
                },


                new Instrument()
                {
                    InstrumentId = 6698,
                    Exchange = "LSE",
                    SegmentCode = "ASQ1",
                    IsinCode = "IE0000020408",
                    MinimumOrderSize = 1,
                    SecurityType = "DE",
                    Tidm = "ABBY",
                    Description = "ABBEY",
                    Sedol = "2040",
                    Currency = "GBX",
                    IssuerCode = "ABGE",
                    IssuerName = "ABBEY PLC",
                    QuotationUnit = 1,
                    Sector = "AMQ1"

                },

                new Instrument()
                {
                    InstrumentId = 3289,
                    Exchange = "LSE",
                    SegmentCode = "SSQ3",
                    IsinCode = "GB0002559291",
                    MinimumOrderSize = 1,
                    SecurityType = "DE",
                    Tidm = "BAE",
                    Description = "BEALE PLC",
                    Sedol = "255929",
                    Currency = "GBX",
                    IssuerCode = "BEAPP",
                    IssuerName = "BEALE PLC",
                    QuotationUnit = 1,
                    Sector = "SQQ3"
                },
                new Instrument()
                {
                    InstrumentId = 4753,
                    Exchange = "FTSE100",
                    SegmentCode = "SET0",
                    IsinCode = "GB0031348658",
                    MinimumOrderSize = 1,
                    SecurityType = "DE",
                    Tidm = "BARC",
                    Description = "BARCLAYS",
                    Sedol = "3134865",
                    Currency = "GBX",
                    IssuerCode = "BASDE",
                    IssuerName = "BARCLAYS PLC",
                    QuotationUnit = 1,
                    Sector = "FE00"
                },
                new Instrument()
                {
                    InstrumentId = 2631,
                    Exchange = "LSE",
                    SegmentCode = "AIMI",
                    IsinCode = "CY1010102113",
                    MinimumOrderSize = 1,
                    SecurityType = "DE",
                    Tidm = "HLS",
                    Description = "HELESI",
                    Sedol = "B1GKDY5",
                    Currency = "GBX",
                    IssuerCode = "HEHLS",
                    IssuerName = "HELESI PLC",
                    QuotationUnit = 1,
                    Sector = "AIMT"
                },
                new Instrument()
                {
                    InstrumentId = 105027,
                    Exchange = "LSE",
                    SegmentCode = "UKCP",
                    IsinCode = "XS0517466198",
                    MinimumOrderSize = 1000,
                    SecurityType = "ML",
                    Tidm = "LBG1",
                    Description = "LLOYDS 5.375%",
                    Sedol = "B3LD6G6",
                    Currency = "GBP",
                    IssuerCode = "LLTSB",
                    IssuerName = "LLOYDS TSB BANK PLC",
                    QuotationUnit = 100,
                    Sector = "UKC2"
                },
                new Instrument()
                {
                    InstrumentId = 6147,
                    Exchange = "FTSE100",
                    SegmentCode = "SET0",
                    IsinCode = "GB00B16GWD56",
                    MinimumOrderSize = 1,
                    SecurityType = "DE",
                    Tidm = "VOD",

                    Description = "VODAFONE GRP.",
                    Sedol = "B16GWD5",
                    Currency = "GBX",
                    IssuerCode = "VOVOD",
                    IssuerName = "VODAFONE GROUP PLC",
                    QuotationUnit = 1,
                    Sector = "FE00"
                    //,,,,1,,,,,,,,1,

                },
                new Instrument()
                {
                    InstrumentId = 8939,
                    Exchange = "LSE",
                    SegmentCode = "SSX4",
                    IsinCode = "US92343V1044",
                    MinimumOrderSize = 1,
                    SecurityType = "IE",
                    Tidm = "VZC",
                    Description = "VERIZON COMMS.",
                    Sedol = "89560",
                    Currency = "USD",
                    IssuerCode = "VEZON",
                    IssuerName = "VERIZON COMMUNICATIONS",
                    QuotationUnit = 1,
                    Sector = "SXSN"
                },
                new Instrument()
                {
                    InstrumentId = 6347,
                    Exchange = "LSE",
                    SegmentCode = "AIM",
                    IsinCode = "GB00B1FQDL10",
                    MinimumOrderSize = 1,
                    SecurityType = "DE",
                    Tidm = "ZOO",
                    Description = "ZOO DIGITAL",
                    Sedol = "B1FQDL1",
                    Currency = "GBX",
                    IssuerCode = "ZOZOO",
                    IssuerName = "ZOO DIGITAL GROUP PLC",
                    QuotationUnit = 1,
                    Sector = "AIM"
                });
        }
    }
}
