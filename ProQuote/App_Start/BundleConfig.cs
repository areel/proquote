﻿using System.Web;
using System.Web.Optimization;

namespace ProQuote
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                "~/Scripts/jquery-{version}.js"));


            bundles.Add(new ScriptBundle("~/bundles/angularJS").Include(
                "~/Scripts/angular.js",
                "~/Scripts/angular-animate.js",
                "~/Scripts/angular-route.js"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap.js"));
            bundles.Add(new ScriptBundle("~/bundles/ProQouteOrderEntryApp").Include(
                        "~/Content/AppFramework/apps/OrderEntryApp.js",
                         "~/Scripts/ng-grid-{version}.js",
                         "~/Content/AppFramework/controllers/homeController.js",
                         "~/Content/AppFramework/controllers/orderBookController.js",
                         "~/Content/AppFramework/controllers/orderEntryController.js",
                         "~/Content/AppFramework/controllers/notesController.js",
                         "~/Content/AppFramework/services/dataService.js",
                         "~/Content/AppFramework/directives/placement/placement.js",
                         "~/Content/AppFramework/directives/buySellButton.js",
                         "~/Scripts/spin.js",
                         "~/Scripts/ladda.js",
                         "~/Scripts/toaster.js"
                         
                        ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/bootstrap.css", "~/Content/ng-grid.css", "~/Content/site.css", "~/Content/ladda-themeless.css", "~/Content/toaster.css"));

        }
    }
}