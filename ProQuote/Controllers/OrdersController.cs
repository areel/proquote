﻿using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using ProQuote.Domain;

namespace ProQuote.Controllers
{
    public class OrdersController : ApiController
    {

        private  IDataSource DataSource { get; set; }
        public OrdersController(IDataSource dataSource)
        {
            DataSource = dataSource;
        }

        // GET api/orders
        public IQueryable<Order> Get()
        {
            var results = DataSource.Orders;
            return results;
        }

        // POST api/order
        public HttpResponseMessage Post(Order payload)
        {
            return SaveChanges(payload);
        }
        //public void Post([FromBody]string value)


        private HttpResponseMessage SaveChanges(Order payload )
        {
            // todo introduce IRepo move mapping out.
            try
            {
                DataSource.Orders.Add(payload);
                DataSource.Save();
                var response = new HttpResponseMessage(HttpStatusCode.Created);
                return response;
            }
            catch (DataException)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
        }


    }
}
