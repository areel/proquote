﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ProQuote.Domain;

namespace ProQuote.Controllers
{
    public class HomeController : Controller
    {

        private IDataSource dataSource;

        public HomeController(IDataSource aDataSource)
        {
            dataSource = aDataSource;
        }
        public ActionResult Index()
        {

            var allInstruments = dataSource.Instruments;

            return View(allInstruments);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your app description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
