﻿using System.Linq;
using System.Web.Http;
using ProQuote.Domain;

namespace ProQuote.Controllers
{

    public class InstrumentsController : ApiController
    {
        private  IDataSource DataSource { get; set; }
        public InstrumentsController(IDataSource dataSource)
        {
            DataSource = dataSource;
        }

        public IQueryable<Instrument> Get()
        {
            var results = DataSource.Instruments;
            return results;
        }
    }
}
