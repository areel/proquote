﻿using System.Data.Entity;
using System.Linq;
using ProQuote.Domain;

namespace ProQuote.Infrastructure
{
    public class DataSource : DbContext, IDataSource
    {

        public DataSource():base("ProQuoteConnection"){}

        public DbSet<Instrument> Instruments { get; set; }
        public DbSet<Order> Orders { get; set; }
        
        void IDataSource.Save()
        {
            SaveChanges();
        }
    }
}