using System.Web.Http;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using ProQuote.Domain;
using ProQuote.Infrastructure;
using Unity.Mvc4;

namespace ProQuote
{
  public static class Bootstrapper
  {
    public static IUnityContainer Initialise()
    {
      var container = BuildUnityContainer();

      DependencyResolver.SetResolver(new UnityDependencyResolver(container));

      // register dependency resolver for WebAPI 
      GlobalConfiguration.Configuration.DependencyResolver = new Unity.WebApi.UnityDependencyResolver(container);


      return container;
    }

    private static IUnityContainer BuildUnityContainer()
    {
      var container = new UnityContainer();

      container.RegisterType<IDataSource, DataSource>();
      RegisterTypes(container);

      return container;
    }

    public static void RegisterTypes(IUnityContainer container)
    {
    
    }
  }
}