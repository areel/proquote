﻿
using System.ComponentModel.DataAnnotations;

namespace ProQuote.Domain
{
    public class Order
    {
        [Key]
        public virtual int? OrderId { get; set; }
        public virtual int? InstrumentId { get; set; }
        public virtual int Quantity { get; set; }
        public virtual string QuantityUnit { get; set; }
        public virtual string Side { get; set; }
        public virtual string OrderType { get; set; }
        public virtual string TimeInForce { get; set; }
        public virtual string AlternativeSettlementCurrency { get; set; }
    }
}
