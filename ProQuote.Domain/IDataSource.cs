﻿using System.Data.Entity;
using System.Linq;

namespace ProQuote.Domain
{
    public interface IDataSource
    {
        DbSet<Instrument> Instruments { get; }
        DbSet<Order> Orders { get; }

        void Save();
    }
}
