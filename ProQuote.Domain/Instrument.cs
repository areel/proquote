﻿
using System.ComponentModel.DataAnnotations;

namespace ProQuote.Domain
{
    public class Instrument
    {
        [Key]
        public virtual int InstrumentId { get; set; }
        public virtual string Exchange { get; set; }
        public virtual string SegmentCode { get; set; }
        public virtual string IsinCode { get; set; }
        public virtual int MinimumOrderSize { get; set; }
        public virtual string SecurityType { get; set; }
        public virtual string Tidm { get; set; }
        public virtual string Description { get; set; }
        public virtual string Sedol { get; set; }
        public virtual string Currency { get; set; }
        public virtual string IssuerCode { get; set; }
        public virtual string IssuerName { get; set; }
        public virtual int QuotationUnit { get; set; }
        public virtual string Sector { get; set; }
    }
}
